USE [master]
GO
/****** Object:  Database [QuestionnaireAppDB]    Script Date: 03-02-2021 18:23:56 ******/
CREATE DATABASE [QuestionnaireAppDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuestionnaireAppDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\QuestionnaireAppDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QuestionnaireAppDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\QuestionnaireAppDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [QuestionnaireAppDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuestionnaireAppDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuestionnaireAppDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuestionnaireAppDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuestionnaireAppDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QuestionnaireAppDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuestionnaireAppDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET RECOVERY FULL 
GO
ALTER DATABASE [QuestionnaireAppDB] SET  MULTI_USER 
GO
ALTER DATABASE [QuestionnaireAppDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuestionnaireAppDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuestionnaireAppDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuestionnaireAppDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QuestionnaireAppDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'QuestionnaireAppDB', N'ON'
GO
ALTER DATABASE [QuestionnaireAppDB] SET QUERY_STORE = OFF
GO
USE [QuestionnaireAppDB]
GO
/****** Object:  User [satest]    Script Date: 03-02-2021 18:23:57 ******/
CREATE USER [satest] FOR LOGIN [satest] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer](
	[AnswerID] [int] IDENTITY(1,1) NOT NULL,
	[AnswerText] [varchar](20) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InputType]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InputType](
	[InputTypeID] [int] IDENTITY(1,1) NOT NULL,
	[InputType] [varchar](20) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_InputType] PRIMARY KEY CLUSTERED 
(
	[InputTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Previlege]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Previlege](
	[PrevilegeID] [int] IDENTITY(1,1) NOT NULL,
	[Controller] [varchar](50) NULL,
	[Action] [varchar](50) NULL,
	[Status] [int] NULL,
	[DisplayName] [varchar](50) NULL,
	[GroupName] [varchar](50) NULL,
 CONSTRAINT [PK_Previlege] PRIMARY KEY CLUSTERED 
(
	[PrevilegeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[QuestionID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionText] [varchar](250) NULL,
	[Status] [int] NULL,
	[QuestionHelpText] [varchar](250) NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Questionnaire]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Questionnaire](
	[QuestionnaireID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionnaireName] [varchar](50) NULL,
	[Status] [int] NULL,
	[Duration] [int] NULL,
 CONSTRAINT [PK_Questionnaire] PRIMARY KEY CLUSTERED 
(
	[QuestionnaireID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionnaireMapping]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionnaireMapping](
	[QuestionnaireMappingID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionnaireID] [int] NULL,
	[QuestionID] [int] NULL,
	[AnswerID] [int] NULL,
	[InputTypeID] [int] NULL,
	[OrderNumber] [int] NULL,
	[Score] [float] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_QuestionnaireMapping] PRIMARY KEY CLUSTERED 
(
	[QuestionnaireMappingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NULL,
	[Previleges] [varchar](250) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](20) NULL,
	[FirstName] [varchar](20) NULL,
	[LastName] [varchar](20) NULL,
	[MiddleName] [varchar](20) NULL,
	[RoleID] [int] NULL,
	[Status] [int] NULL,
	[Password] [varchar](20) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAssessment]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAssessment](
	[UserAssessmentID] [int] NOT NULL,
	[UserID] [int] NULL,
	[QuestionID] [int] NULL,
	[Answer] [varchar](50) NULL,
	[QuestionnaireID] [int] NULL,
 CONSTRAINT [PK_UserAssessment] PRIMARY KEY CLUSTERED 
(
	[UserAssessmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserPrevilege]    Script Date: 03-02-2021 18:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPrevilege](
	[UserPrevilegeID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[Previleges] [varchar](250) NULL,
 CONSTRAINT [PK_UserPrevilege] PRIMARY KEY CLUSTERED 
(
	[UserPrevilegeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [QuestionnaireAppDB] SET  READ_WRITE 
GO
